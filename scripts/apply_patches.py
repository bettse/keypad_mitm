from os.path import join, isfile

Import("env")

PROJ = env["PROJECT_DIR"]
WORKSPACE = env["PROJECT_WORKSPACE_DIR"]

patchflag_path = join(PROJ, ".patching-done")

# patch file only if we didn't do it before
if not isfile(patchflag_path):
    original_file = join(WORKSPACE, "libdeps", "pico", "PN5180 Library")
    patched_file = join(PROJ, "patches", "4.patch")
    print(original_file)
    print(patched_file)

    assert isfile(patched_file)

    env.Execute('patch -d "%s" -i "%s"' % (original_file, patched_file))
    # patch -d "/home/bettse/keypad_mitm/.pio/libdeps/pico/PN5180 Library/" -i "/home/bettse/keypad_mitm/patches/4.patch"
    print("patch %s" % patched_file)
    # env.Execute("touch " + patchflag_path)


    def _touch(path):
        with open(path, "w") as fp:
            fp.write("")

    env.Execute(lambda *args, **kwargs: _touch(patchflag_path))
