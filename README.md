# MITM the keypad of an Automatic Products C series vending machine.

## Dispense

 * The price of all products is set to zero so we can MITM the keypad.  The change receivier has been removed to make room for electronics.
 * 4 byte UID where first byte is "B1", "B2", or "B3"
 * MFC where the last byte of block 62 if "A1", "A2", or "A3"

## Control LEDs with Mifare Classic

Set the contents of block 1 to control [WS2812FX](https://github.com/kitesurfer1404/WS2812FX).  Will revert to default after 5 minutes.

### Format:

Prefix the block with `WS2812FX` (`57 53 32 38 31 32 46 58`), and then provide preferred options: `57 53 32 38 31 32 46 58 MM CC CC CC CC SS SS RR`

#### Mode (1 byte)


0. **Static** - No blinking. Just plain old static light.
1. **Blink** - Normal blinking. 50% on/off time.
2. **Breath** - Does the "standby-breathing" of well known i-Devices. Fixed Speed.
3. **Color Wipe** - Lights all LEDs after each other up. Then turns them in that order off. Repeat.
4. **Color Wipe Inverse** - Same as Color Wipe, except swaps on/off colors.
5. **Color Wipe Reverse** - Lights all LEDs after each other up. Then turns them in reverse order off. Repeat.
6. **Color Wipe Reverse Inverse** - Same as Color Wipe Reverse, except swaps on/off colors.
7. **Color Wipe Random** - Turns all LEDs after each other to a random color. Then starts over with another color.
8. **Random Color** - Lights all LEDs in one random color up. Then switches them to the next random color.
9. **Single Dynamic** - Lights every LED in a random color. Changes one random LED after the other to a random color.
10. **Multi Dynamic** - Lights every LED in a random color. Changes all LED at the same time to new random colors.
11. **Rainbow** - Cycles all LEDs at once through a rainbow.
12. **Rainbow Cycle** - Cycles a rainbow over the entire string of LEDs.
13. **Scan** - Runs a single pixel back and forth.
14. **Dual Scan** - Runs two pixel back and forth in opposite directions.
15. **Fade** - Fades the LEDs on and (almost) off again.
16. **Theater Chase** - Theatre-style crawling lights. Inspired by the Adafruit examples.
17. **Theater Chase Rainbow** - Theatre-style crawling lights with rainbow effect. Inspired by the Adafruit examples.
18. **Running Lights** - Running lights effect with smooth sine transition.
19. **Twinkle** - Blink several LEDs on, reset, repeat.
20. **Twinkle Random** - Blink several LEDs in random colors on, reset, repeat.
21. **Twinkle Fade** - Blink several LEDs on, fading out.
22. **Twinkle Fade Random** - Blink several LEDs in random colors on, fading out.
23. **Sparkle** - Blinks one LED at a time.
24. **Flash Sparkle** - Lights all LEDs in the selected color. Flashes single white pixels randomly.
25. **Hyper Sparkle** - Like flash sparkle. With more flash.
26. **Strobe** - Classic Strobe effect.
27. **Strobe Rainbow** - Classic Strobe effect. Cycling through the rainbow.
28. **Multi Strobe** - Strobe effect with different strobe count and pause, controlled by speed setting.
29. **Blink Rainbow** - Classic Blink effect. Cycling through the rainbow.
30. **Chase White** - Color running on white.
31. **Chase Color** - White running on color.
32. **Chase Random** - White running followed by random color.
33. **Chase Rainbow** - White running on rainbow.
34. **Chase Flash** - White flashes running on color.
35. **Chase Flash Random** - White flashes running, followed by random color.
36. **Chase Rainbow White** - Rainbow running on white.
37. **Chase Blackout** - Black running on color.
38. **Chase Blackout Rainbow** - Black running on rainbow.
39. **Color Sweep Random** - Random color introduced alternating from start and end of strip.
40. **Running Color** - Alternating color/white pixels running.
41. **Running Red Blue** - Alternating red/blue pixels running.
42. **Running Random** - Random colored pixels running.
43. **Larson Scanner** - K.I.T.T.
44. **Comet** - Firing comets from one end.
45. **Fireworks** - Firework sparks.
46. **Fireworks Random** - Random colored firework sparks.
47. **Merry Christmas** - Alternating green/red pixels running.
48. **Fire Flicker** - Fire flickering effect. Like in harsh wind.
49. **Fire Flicker (soft)** - Fire flickering effect. Runs slower/softer.
50. **Fire Flicker (intense)** - Fire flickering effect. More range of color.
51. **Circus Combustus** - Alternating white/red/black pixels running.
52. **Halloween** - Alternating orange/purple pixels running.
53. **Bicolor Chase** - Two LEDs running on a background color.
54. **Tricolor Chase** - Alternating three color pixels running.
55. **TwinkleFOX** - Lights fading in and out randomly.

Example for christmas lights: `57 53 32 38 31 32 46 58 2F`

#### Color (4 bytes)

Requires mode be specified first

4 bytes `WWRRGGBB`

From the documentation:

```
#define RED        (uint32_t)0xFF0000
#define GREEN      (uint32_t)0x00FF00
#define BLUE       (uint32_t)0x0000FF
#define WHITE      (uint32_t)0xFFFFFF
#define BLACK      (uint32_t)0x000000
#define YELLOW     (uint32_t)0xFFFF00
#define CYAN       (uint32_t)0x00FFFF
#define MAGENTA    (uint32_t)0xFF00FF
#define PURPLE     (uint32_t)0x400080
#define ORANGE     (uint32_t)0xFF3000
#define PINK       (uint32_t)0xFF1493
#define GRAY       (uint32_t)0x101010
#define ULTRAWHITE (uint32_t)0xFFFFFFFF /* for RGBW LEDs */
```

#### Speed (2 bytes)

Requires mode and color be specified first.  Two byte unsigned integer.

From the documentation:

> 10=very fast, 5000=very slow

#### Reverse (boolean)

Requires mode, color, and speed be specified first

 * `00`: forward
 * `01`: reverse

## Development

### Pin mapping

```
A = 26, 27
B = 25, 27
C = 14, 27
D = 12, 27
1 = 25, 26
2 = 12, 13
3 = 14, 25
4 = 13, 25
5 = 13, 14
6 = 12, 25
7 = 12, 14
8 = 12, 26

A = 4, 5
B = 3, 5
C = 2, 5
D = 0, 5
1 = 3, 4
2 = 0, 1
3 = 2, 3
4 = 1, 3
5 = 1, 2
6 = 0, 3
7 = 0, 2
8 = 0, 4
```
