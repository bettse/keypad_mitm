#include "secrets.h"

#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include <Chrono.h>
#include <LibPrintf.h>
#include <PN5180.h>
#include <PN5180ISO14443.h>
#include <WS2812FX.h>
#include <WebServer.h>
#include <WiFi.h>

#define NUM_WINDOW_PIXELS 106
#define WINDOW_OFFSET NUM_WINDOW_PIXELS
#define PIXELS_PER_SHELF 21
#define NUM_SHELF_PIXELS (3 * PIXELS_PER_SHELF)
#define NUM_PIXELS (NUM_WINDOW_PIXELS + NUM_SHELF_PIXELS)

#define WS2812_PIN 14
#define KEY_PIN 15

#define PN5180_NSS 13
#define PN5180_BUSY 22
#define PN5180_RST 9

#define SHELF_LIGHT_TIMEOUT 5000
#define IDLE_ANIMATION_DURATION (5 * 60 * 1000)

#define MFC_BLOCK_SIZE 16
#define KIND_REQA 0
#define KIND_WUPA 1

#define WINDOW_IDLE_SEGMENT 0
#define WINDOW_VEND_SEGMENT 1
#define SHELF_A_SEGMENT_ON 2
#define SHELF_B_SEGMENT_ON 3
#define SHELF_C_SEGMENT_ON 4
#define SHELF_A_SEGMENT_OFF 5
#define SHELF_B_SEGMENT_OFF 6
#define SHELF_C_SEGMENT_OFF 7

union __attribute__((packed)) ActivateResponse {
  uint8_t buffer[10];
  struct {
    uint16_t atqa;
    uint8_t sak;
    union {
      uint8_t uid4[4];
      uint8_t uid7[7];
    } uid;
  } data;
} activateResponse;

const uint8_t outputPins[] = {16, 17, 18, 19, 20, 21};

uint8_t KEY_NONE[] = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH};
uint8_t KEY_A[] = {HIGH, HIGH, HIGH, HIGH, LOW, LOW};
uint8_t KEY_B[] = {HIGH, HIGH, HIGH, LOW, HIGH, LOW};
uint8_t KEY_C[] = {HIGH, HIGH, LOW, HIGH, HIGH, LOW};
uint8_t KEY_D[] = {LOW, HIGH, HIGH, HIGH, HIGH, LOW};
uint8_t KEY_1[] = {HIGH, HIGH, HIGH, LOW, LOW, HIGH};
uint8_t KEY_2[] = {LOW, LOW, HIGH, HIGH, HIGH, HIGH};
uint8_t KEY_3[] = {HIGH, HIGH, LOW, LOW, HIGH, HIGH};
uint8_t KEY_4[] = {HIGH, LOW, HIGH, LOW, HIGH, HIGH};
uint8_t KEY_5[] = {HIGH, LOW, LOW, HIGH, HIGH, HIGH};
uint8_t KEY_6[] = {LOW, HIGH, HIGH, LOW, HIGH, HIGH};
uint8_t KEY_7[] = {LOW, HIGH, LOW, HIGH, HIGH, HIGH};
uint8_t KEY_8[] = {LOW, HIGH, HIGH, HIGH, LOW, HIGH};

int active = LOW;

const uint8_t inputPins[] = {3, 4, 5, 6, 7, 8};

const uint8_t indexButtonMap[6][6] = {
    {0, '2', '7', '6', '8', 'D'}, {'2', 0, '5', '4', 0, 0},
    {'7', '5', 0, '3', 0, 'C'},   {'6', '4', '3', 0, '1', 'B'},
    {'8', 0, 0, '1', 0, 'A'},     {'D', 0, 'C', 'B', 'A', 0}};

int status = WL_IDLE_STATUS; // the Wifi radio's status
bool nfcErrorFlag = false;

WebServer server(80);
StaticJsonDocument<250> jsonDocument;
PN5180ISO14443 nfc(PN5180_NSS, PN5180_BUSY, PN5180_RST, SPI1);
Chrono shelfLightTimer;
Chrono idleAnimationReset;
WS2812FX ws2812fx = WS2812FX(NUM_PIXELS, WS2812_PIN, NEO_GRB + NEO_KHZ800);

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)(r) << 8) | ((uint32_t)(g) << 16) | (uint32_t)(b);
}

static inline bool isMFC(uint16_t atqa, uint8_t sak) {
  return (atqa == 0x0004 && sak == 0x08);
}

void pressKey(char key) {
  Serial.print("Press key: ");
  Serial.println(key);

  uint8_t *keymap[] = {NULL,  KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6,
                       KEY_7, KEY_8, NULL,  NULL,  NULL,  NULL,  NULL,
                       NULL,  NULL,  NULL,  KEY_A, KEY_B, KEY_C, KEY_D};

  uint8_t *pins = keymap[key - '0'];

  if (pins) {
    if (key == 'A' || key == 'B' || key == 'C') {
      disable_shelf_lights();
      printf("Swap segment %d -> %d\n", key - 'A' + SHELF_A_SEGMENT_OFF, key - 'A' + SHELF_A_SEGMENT_ON);
      ws2812fx.swapActiveSegment(key - 'A' + SHELF_A_SEGMENT_OFF, key - 'A' + SHELF_A_SEGMENT_ON);
      shelfLightTimer.restart();
    } else if (key > '0' && key < '7') {
      if (ws2812fx.isActiveSegment(SHELF_A_SEGMENT_ON) ||
          ws2812fx.isActiveSegment(SHELF_B_SEGMENT_ON) ||
          ws2812fx.isActiveSegment(SHELF_C_SEGMENT_ON)) {
        printf("Swap segment %d -> %d\n", WINDOW_IDLE_SEGMENT,
               WINDOW_VEND_SEGMENT);
        ws2812fx.swapActiveSegment(WINDOW_IDLE_SEGMENT, WINDOW_VEND_SEGMENT);
        shelfLightTimer.restart();
      }
    }

    for (uint8_t i = 0; i < sizeof(outputPins); i++) {
      digitalWrite(outputPins[i], pins[i]);
    }

    // TODO: determine best value
    delay(500);
    for (uint8_t i = 0; i < sizeof(outputPins); i++) {
      digitalWrite(outputPins[i], KEY_NONE[i]);
    }
  }
}

// Also the healthcheck endpoint
void getRoot() {
  // Serial.println("Get::Root");
  server.send(200, "application/json", "{}");
}

void handle_NotFound() {
  Serial.println("NotFound");
  server.send(404, "application/json", "{}");
}

void postVend() {
  Serial.println("POST::Vend");

  // Disable web requests
  if (digitalRead(KEY_PIN) != active) {
    server.send(423, "application/json", "{\"state\": \"offline\"}");
    return;
  }

  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    JsonVariant selection = jsonDocument["selection"];
    if (selection.isNull()) {
      server.send(422, "application/json", "{}");
      return;
    }
    jsonDocument.clear(); // Clear json buffer

    String value = selection.as<String>();
    char shelf = value.charAt(0);
    char column = value.charAt(1);
    // TODO: validate:
    //  A1, A2, A3
    //  B1, B2, B3
    //  C1, C2, C3, C4, C5, C7

    if (shelf == 'A' || shelf == 'B') {
      if (column > '0' && column < '4') {
        Serial.print("Vend ");
        Serial.println(value);
        pressKey(shelf);
        pressKey(column);
        server.send(204);
        return;
      }
    }
    if (shelf == 'C') {
      if (column > '0' && column < '8') {
        Serial.print("Vend ");
        Serial.println(value);
        pressKey(shelf);
        pressKey(column);
        server.send(204);
        return;
      }
    }
    server.send(404);
  }

  server.send(422, "application/json", "{}");
}

void postKeypad() {
  Serial.println("POST::Keypad");
  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    JsonVariant press = jsonDocument["press"];
    if (press.isNull()) {
      server.send(422, "application/json", "{}");
      return;
    }
    jsonDocument.clear(); // Clear json buffer

    String key = press.as<String>();
    Serial.print("Keypad ");
    Serial.println(key);
    pressKey(key.charAt(0));
    server.send(204);
    return;
  }

  server.send(422, "application/json", "{}");
}

void setupWebserver() {
  server.on("/", HTTP_GET, getRoot);
  server.on("/vend", HTTP_POST, postVend);
  server.on("/keypad", HTTP_POST, postKeypad);
  // TODO:
  // * Healthcheck/status
  // * LED Control
  // * enable/disable vending

  server.onNotFound(handle_NotFound);
  server.enableDelay(1);
  server.enableCORS();
  server.enableCrossOrigin();
  server.begin();
  Serial.println("Webserver started");
}

void setupNFC() {
  SPI1.setSCK(10);
  SPI1.setTX(11);
  SPI1.setRX(12);
  SPI1.setCS(PN5180_NSS);

  nfc.begin();
  Serial.println(F("PN5180 Hard-Reset..."));
  nfc.reset();

  Serial.println(F("Reading product version..."));
  uint8_t productVersion[2];
  nfc.readEEprom(PRODUCT_VERSION, productVersion, sizeof(productVersion));
  Serial.print(F("Product version="));
  Serial.print(productVersion[1]);
  Serial.print(".");
  Serial.println(productVersion[0]);

  if (0xff ==
      productVersion[1]) { // if product version 255, the initialization failed
    Serial.println(F("Initialization failed!?"));
    Serial.println(F("Press reset to restart..."));
    Serial.flush();
    exit(-1); // halt
  }

  Serial.println(F("----------------------------------"));
  Serial.println(F("Reading firmware version..."));
  uint8_t firmwareVersion[2];
  nfc.readEEprom(FIRMWARE_VERSION, firmwareVersion, sizeof(firmwareVersion));
  Serial.print(F("Firmware version="));
  Serial.print(firmwareVersion[1]);
  Serial.print(".");
  Serial.println(firmwareVersion[0]);

  Serial.println(F("----------------------------------"));
  Serial.println(F("Enable RF field..."));
  nfc.setupRF();
}

void resetWindowAnimation() {
  ws2812fx.setSegment(WINDOW_IDLE_SEGMENT, 0, NUM_WINDOW_PIXELS - 1, FX_MODE_THEATER_CHASE_RAINBOW);
}

void setup() {
  Serial.begin(115200);

  for (uint8_t i = 0; i < sizeof(inputPins); i++) {
    pinMode(inputPins[i], INPUT_PULLUP);
  }

  for (uint8_t i = 0; i < sizeof(outputPins); i++) {
    pinMode(outputPins[i], OUTPUT);
  }

  pinMode(KEY_PIN, INPUT_PULLUP);

#ifdef ENABLE_WIFI
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(WIFI_SSID);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    // wait 10 seconds for connection:
    delay(10000);
  }

  Serial.println("connected...yeey :)");

  setupWebserver();
#endif

  setupNFC();
}

void setup1() {
  ws2812fx.init();
  ws2812fx.setBrightness(60);

  resetWindowAnimation();
  ws2812fx.setIdleSegment(WINDOW_VEND_SEGMENT, 0, NUM_WINDOW_PIXELS - 1,
                          FX_MODE_BREATH, GREEN, 200, true);

  ws2812fx.setSegment(SHELF_A_SEGMENT_OFF, WINDOW_OFFSET,
                          WINDOW_OFFSET + PIXELS_PER_SHELF - 1, FX_MODE_STATIC,
                          BLACK, 2000);
  ws2812fx.setSegment(SHELF_B_SEGMENT_OFF, WINDOW_OFFSET + PIXELS_PER_SHELF,
                          WINDOW_OFFSET + PIXELS_PER_SHELF * 2 - 1,
                          FX_MODE_STATIC, BLACK, 2000);
  ws2812fx.setSegment(SHELF_C_SEGMENT_OFF, WINDOW_OFFSET + PIXELS_PER_SHELF * 2,
                          NUM_PIXELS - 1, FX_MODE_STATIC, BLACK, 2000);

  ws2812fx.setIdleSegment(SHELF_A_SEGMENT_ON, WINDOW_OFFSET,
                          WINDOW_OFFSET + PIXELS_PER_SHELF - 1, FX_MODE_STATIC,
                          WHITE, 2000);
  ws2812fx.setIdleSegment(SHELF_B_SEGMENT_ON, WINDOW_OFFSET + PIXELS_PER_SHELF,
                          WINDOW_OFFSET + PIXELS_PER_SHELF * 2 - 1,
                          FX_MODE_STATIC, WHITE, 2000);
  ws2812fx.setIdleSegment(SHELF_C_SEGMENT_ON, WINDOW_OFFSET + PIXELS_PER_SHELF * 2,
                          NUM_PIXELS - 1, FX_MODE_STATIC, WHITE, 2000);


  ws2812fx.start();
}

uint8_t buttonCheck() {
  for (uint8_t i = 0; i < sizeof(inputPins); i++) {
    // A pin is active, look for second pin
    if (digitalRead(inputPins[i]) == active) {
      for (uint8_t j = i + 1; j < sizeof(inputPins); j++) {
        if (digitalRead(inputPins[j]) == active) {
          uint8_t button = indexButtonMap[i][j];
          if (button) {
            return button;
          } else {
            printf("(%i, %i) mapped to invalid button\n", i, j);
          }
          return button;
        }
      }
    }
  }
  return 0;
}

void disable_shelf_lights() {
  for (uint8_t i = SHELF_A_SEGMENT_ON; i <= SHELF_C_SEGMENT_ON; i++) {
    if (ws2812fx.isActiveSegment(i)) {
      printf("Swap segment %d -> %d\n", i, i + 3);
      ws2812fx.swapActiveSegment(i, i + 3);
    }
  }
}

#define MAGIC_CMD_WIPE (0x41)
#define MAGIC_ACK (0x0A)
#define MAGIC_BUFFER_SIZE (32)
bool mfcMagicWipe() {
  uint8_t cmd[MAGIC_BUFFER_SIZE] = {0};
  cmd[0] = MAGIC_CMD_WIPE;
  bool success = nfc.sendData(cmd, sizeof(cmd));
  if (success) {
    //https://github.com/RfidResearchGroup/proxmark3/blob/master/doc/magic_cards_notes.md#magic-commands
    delay(2000);
    success = nfc.readData(sizeof(cmd), cmd);
    Serial.print("Magic wipe resp:");
    for (uint8_t i = 0; i < MAGIC_BUFFER_SIZE; i++) {
      printf("%02x", cmd[i]);
    }
    Serial.println();
    return cmd[0] == MAGIC_ACK;
  }
  return success;
}

bool mfcAuth(uint8_t uid[4], uint8_t blockNum) {
  uint8_t key[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
  uint8_t keyType = 0; // 0: A, 1: B
  return nfc.mifareAuth(uid, blockNum, key, keyType);
}

uint8_t readMagicByte(uint8_t uid[4]) {
  uint8_t blockNum = 62;
  if (!mfcAuth(uid, blockNum)) {
    printf("Auth failed for block %d\n", blockNum);
    return 0;
  }
  uint8_t buffer[MFC_BLOCK_SIZE] = {0};
  bool success = nfc.mifareBlockRead(blockNum, buffer);
  if (success) {
    Serial.print("Magic block:");
    for (uint8_t i = 0; i < MFC_BLOCK_SIZE; i++) {
      printf("%02x", buffer[i]);
    }
    Serial.println();
    return buffer[MFC_BLOCK_SIZE - 1];
  }
  return 0;
}

bool nfcLEDControl(uint8_t uid[4]) {
  char ledPrefix[] = "WS2812FX";
  uint8_t prefixLength = 8;//Omit null byte
  uint8_t blockNum = 1;
  if (!mfcAuth(uid, blockNum)) {
    printf("Auth failed for block %d\n", blockNum);
    return false;
  }
  uint8_t buffer[MFC_BLOCK_SIZE] = {0};
  bool success = nfc.mifareBlockRead(blockNum, buffer);
  if (success) {
    Serial.print("LED block:");
    for (uint8_t i = 0; i < MFC_BLOCK_SIZE; i++) {
      printf("%02x", buffer[i]);
    }
    Serial.println();

    if(memcmp(ledPrefix, buffer, prefixLength) == 0) {
      // https://github.com/kitesurfer1404/WS2812FX#effects
      uint8_t mode = buffer[prefixLength];
      uint32_t *color = (uint32_t*)(buffer + prefixLength + 1);
      uint16_t *speed = (uint16_t*)(buffer + prefixLength + 5);
      bool reverse = buffer[prefixLength + 6] != 0;
      printf("LED: %d %08x %04x %s\n", mode, *color, *speed, reverse ? "reverse" : "");
      //setSegment(uint8_t n, uint16_t start, uint16_t stop, uint8_t mode, uint32_t color, uint16_t speed, bool reverse),
      if (*color) {
        if (*speed) {
          ws2812fx.setSegment(WINDOW_IDLE_SEGMENT, 0, NUM_WINDOW_PIXELS - 1, mode, *color, *speed, reverse);
        } else {
          ws2812fx.setSegment(WINDOW_IDLE_SEGMENT, 0, NUM_WINDOW_PIXELS - 1, mode, *color);
        }
      } else {
          ws2812fx.setSegment(WINDOW_IDLE_SEGMENT, 0, NUM_WINDOW_PIXELS - 1, mode);
      }
      return true;
    }
  }
  return false;
}

void nfc_loop() {
  if (nfcErrorFlag) {
    uint32_t irqStatus = nfc.getIRQStatus();

    if (0 == (RX_SOF_DET_IRQ_STAT & irqStatus)) { // no card detected
      Serial.println(F("*** No card detected!"));
    }

    nfc.reset();
    nfc.setupRF();

    nfcErrorFlag = false;
    delay(10);
  }

  int8_t uidLength = nfc.activateTypeA(activateResponse.buffer, KIND_REQA);
  if (uidLength > 0) {
    for (uint8_t i = 0; i < 10; i++) {
      printf("%02x ", activateResponse.buffer[i]);
    }
    Serial.println();
    uint16_t atqa = activateResponse.data.atqa;
    uint8_t sak = activateResponse.data.sak;

    printf("ISO-14443 card found, ATQA=%04x SAK=%02x\n", atqa, sak);
    if (uidLength == 4) {
      printf("UID=%02x%02x%02x%02x\n", activateResponse.data.uid.uid4[0],
             activateResponse.data.uid.uid4[1],
             activateResponse.data.uid.uid4[2],
             activateResponse.data.uid.uid4[3]);
      Serial.println(F("----------------------------------"));
    } else if (uidLength == 7) {
      printf("UID=%02x%02x%02x%02x%02x%02x%02x\n",
             activateResponse.data.uid.uid4[0],
             activateResponse.data.uid.uid4[1],
             activateResponse.data.uid.uid4[2],
             activateResponse.data.uid.uid4[3],
             activateResponse.data.uid.uid4[4],
             activateResponse.data.uid.uid4[5],
             activateResponse.data.uid.uid4[6]);
      Serial.println(F("----------------------------------"));
    }

    if (isMFC(atqa, sak)) {
      bool usedCard = false;
      char keys[3] = {0};
      uint8_t *uid4 = activateResponse.data.uid.uid4;

      // usedCard = mfcMagicWipe();

      // 1. Check for magic byte
      do {
        uint8_t b = readMagicByte(uid4);
        if (b == 0x00 || b == 0xff) {
          break;
        }

        sprintf(keys, "%02X", b);
        if (keys[0] == 'A') {
          // Limit this method to only the 'A' shelf.
          pressKey(keys[0]);
          pressKey(keys[1]);
          usedCard = true;
        }
      } while(false);

      // 2. Check for LED control
      if (!usedCard) {
        usedCard = nfcLEDControl(uid4);
      }

      // 3. Check for UID
      if (!usedCard) {
        size_t keyLength = snprintf(keys, sizeof(keys), "%02X", uid4[0]);
        if (keyLength > 1 && keys[0]  == 'B') {
          // Limit this method to only the 'B' shelf.
          pressKey(keys[0]);
          pressKey(keys[1]);
          usedCard = true;
        }
      }

    }
  }
}

void loop() {
#ifdef ENABLE_WIFI
  delay(1);
  server.handleClient();
#endif

  if (digitalRead(KEY_PIN) == active) {
    nfc_loop();
  }

  if (digitalRead(KEY_PIN) == active) {
    uint8_t button = buttonCheck();
    if (button) {
      Serial.print("Keypad: ");
      Serial.println((char)button);
      pressKey((char)button);
    }
  }
}

void loop1() {
  ws2812fx.service();
  if (shelfLightTimer.hasPassed(SHELF_LIGHT_TIMEOUT)) {
    shelfLightTimer.restart();
    if (ws2812fx.isActiveSegment(WINDOW_VEND_SEGMENT)) {
      printf("Swap segment %d -> %d\n", WINDOW_VEND_SEGMENT,
             WINDOW_IDLE_SEGMENT);
      ws2812fx.swapActiveSegment(WINDOW_VEND_SEGMENT, WINDOW_IDLE_SEGMENT);
    }
    disable_shelf_lights();
  }

  if (digitalRead(KEY_PIN) == active) {
    if (idleAnimationReset.hasPassed(IDLE_ANIMATION_DURATION)) {
      idleAnimationReset.restart();
      resetWindowAnimation();
    }
  } else {
    //TODO: Track key 'falling'
    ws2812fx.setSegment(WINDOW_IDLE_SEGMENT, 0, NUM_WINDOW_PIXELS - 1, FX_MODE_STATIC, BLACK);
  }
}
