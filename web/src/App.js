import React, { useState, useEffect } from "react";

import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ListGroup from 'react-bootstrap/ListGroup';

import './App.css';


function App() {
  const [selection, setSelection] = useState(null);

  useEffect(() => {
    async function vend(selection) {
      try {
        await fetch('/vend', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({selection}),
        });
      } catch (e) {
        console.log(e);
      }
      setSelection(null)
    }
    if (selection) {
      vend(selection)
    }
  }, [selection]);

  const rowMaker = (name) => {
    return (
      <ListGroup.Item
        action
        active={name === selection}
        key={name}
        onClick={() => setSelection(name)}
      >{name}</ListGroup.Item>
    );
  }

  return (
    <div className="App">
      <Navbar collapseOnSelect expand="sm" bg="secondary" variant="secondary">
        <Navbar.Brand>Vending Machine</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      </Navbar>
      <Container className="pt-1" fluid>
        <Row className="justify-content-center text-center">
          <Col>
            <ListGroup horizontal className="justify-content-center pt-3">
              {['A1', 'A2', 'A3'].map(rowMaker)}
            </ListGroup>
            <ListGroup horizontal className="justify-content-center pt-3">
              {['B1', 'B2', 'B3'].map(rowMaker)}
            </ListGroup>
            <ListGroup horizontal className="justify-content-center pt-3">
              {['C1', 'C2', 'C3', 'C4', 'C5', 'C6'].map(rowMaker)}
            </ListGroup>
          </Col>
        </Row>
      </Container>
      <footer className="footer font-small mx-auto pt-5">
        <Container fluid className="text-center">
          <Row>
            <Col>
              <small className="text-muted">
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default App;
